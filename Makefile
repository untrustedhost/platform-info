makepath := $(abspath $(lastword $(MAKEFILE_LIST)))
OUTPUTDIR := $(dir $(makepath))
GPG2 := $(shell which gpg2)
GPG := $(shell which gpg)
ifeq ($(GPG2),)
GPGBIN := $(GPG)
else
GPGBIN := $(GPG2)
endif

MAKESELF_VER := 2.4.0

.DEFAULT_GOAL := $(OUTPUTDIR)install.run

$(OUTPUTDIR)install.run:: Makefile $(shell find src -type f)

# this sets up a scratch directory
ifeq ($(tmpdir),)
%:
	@tmpdir=`mktemp --tmpdir -d` ; \
	trap 'rm -rf "$$tmpdir"' EXIT ; \
	$(MAKE) -f $(makepath) --no-print-directory tmpdir=$$tmpdir $@
else

.DELETE_ON_ERROR:

cache/codesign-ownertrust: ./support/codesign-ownertrust.sh
	./support/codesign-ownertrust.sh

cache/makeself-$(MAKESELF_VER).run: cache/codesign-ownertrust
	curl -L -o $(tmpdir)/makeself.run 'https://github.com/megastep/makeself/releases/download/release-$(MAKESELF_VER)/makeself-$(MAKESELF_VER).run'
	$(GPGBIN) --verify signatures/makeself-$(MAKESELF_VER).run.sig $(tmpdir)/makeself.run
	chmod +x $(tmpdir)/makeself.run
	mv $(tmpdir)/makeself.run cache/makeself-$(MAKESELF_VER).run

$(tmpdir)/makeself: cache/makeself-$(MAKESELF_VER).run
	./cache/makeself-$(MAKESELF_VER).run --target $(tmpdir)/makeself --keep --noexec

$(OUTPUTDIR)install.run::
	$(MAKE) $(tmpdir)/makeself
	$(tmpdir)/makeself/makeself.sh src $(OUTPUTDIR)install.run "untrustedhost/platform-info installer" ./install.sh
	touch $(OUTPUTDIR)install.run

# end of the tmpdir wrapper!
endif
