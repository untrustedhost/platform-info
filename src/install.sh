#!/usr/bin/env bash

# we expect to be 'in' the src/ directory
set -eux

# scripts
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/scripts
install --mode=0755 --owner=0 --group=0 ./platform-info.sh /usr/lib/untrustedhost/scripts/platform-info.sh

# platform hooks
install --mode=0755 --owner=0 --group=0 --directory /usr/lib/untrustedhost/platform-exec
for f in platform-exec/* ; do
  b="${f#platform-exec/}"
  install --mode=0755 --owner=0 --group=0 "${f}" "/usr/lib/untrustedhost/platform-exec/${b}"
done

# systemd control files
install --mode=0644 --owner=0 --group=0 ./systemd/platform-info.service /etc/systemd/system/platform-info.service

# flip on
systemctl enable platform-info
