#!/usr/bin/env bash

pi_dir=/run/platform-info
ex_dir=/usr/lib/untrustedhost/platform-exec

makewords() {
  local f=''
  # search these files for strings to read out
  for f in /sys/class/dmi/id/board_vendor /sys/class/dmi/id/chassis_vendor \
           /sys/class/dmi/id/bios_version /sys/class/dmi/id/board_version \
           /sys/class/dmi/id/product_name /sys/firmware/devicetree/base/model
  do
    local b='' ; local worddir='' ; local words='' ; local word=''
    if [[ -f "${f}" ]] ; then
    b="${f##*/}"
    worddir="${pi_dir}/${b}_words"
    mkdir -p "${worddir}"
    read -r words < "${f}"
      for word in ${words} ; do
        word="${word//(/}"
        word="${word//)/}"
        word="${word//,/}"
        word="${word////}"
        case "${word}" in
          "."|".."|"/"|"+"|"-"|"_"|"\\"|"\\n") : ;;
          *) touch "${worddir}/${word,,}" ;;
        esac
      done
    fi
  done
}

# always create words to key off of.
makewords

# if we have gennetlinks, remove _PLATFORM_ files in /etc/systemd/network.
[[ "${1}" == 'gennetlinks' ]] && {
  rm -rf /etc/systemd/network/*_PLATFORM_*
  rm -rf /etc/dracut.d/platform.conf
}

# run stub scripts with argument
for p in "${pi_dir}"/*/* ; do
  [[ -f "${p}" ]] && {
    t="${p#${pi_dir}/}"
    t="${t////-}"
    [[ -x "${ex_dir}/${t}" ]] && "${ex_dir}/${t}" "${@}"
  }
done

exit 0
